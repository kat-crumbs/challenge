require 'rails_helper'

RSpec.describe QuestionsController do

  describe '#index' do
    it 'should redirect to error if there are no new questions to load' do
      session[:answered_questions] = [1,2,3,4,5,6]
      get :index
      expect(response).to redirect_to :action => :error
    end
  end

  describe '#decision' do
    it 'should increment only dog_counter if question was a dog question and answered with yes' do
      session[:dog_counter] = 3
      session[:cat_counter] = 3
      get :decision, {:id => 2, :decision => 'yes'}
      expect(assigns(:dog_counter)).to eq(4)
      expect(assigns(:cat_counter)).to eq(3)
    end

    it 'should increment only dog_counter if question was a cat question and answered with no' do
      session[:dog_counter] = 2
      session[:cat_counter] = 2
      get :decision, {:id => 1, :decision => 'no'}
      expect(assigns(:dog_counter)).to eq(3)
      expect(assigns(:cat_counter)).to eq(2)
    end

    it 'should increment only cat_counter if question was a cat question and answered with yes' do
      session[:cat_counter] = 3
      session[:dog_counter] = 3
      get :decision, {:id => 1, :decision => 'yes'}
      expect(assigns(:cat_counter)).to eq(4)
      expect(assigns(:dog_counter)).to eq(3)
    end

    it 'should increment only cat_counter if question was a dog question and answered with no' do
      session[:cat_counter] = 2
      session[:dog_counter] = 2
      get :decision, {:id => 2, :decision => 'no'}
      expect(assigns(:cat_counter)).to eq(3)
      expect(assigns(:dog_counter)).to eq(2)
    end

    it 'should redirect to index if more questions should come' do
      session[:answered_questions] = [2,1]
      get :decision, {:id => '/\d', :decision => '/[yes]|[no]'}
      expect(response).to redirect_to :action => :index
    end

    it 'should redirect to result if all questions are asked' do
      session[:answered_questions] = [2,1,3]
      get :decision, {:id => '/\d', :decision => '/[yes]|[no]'}
      expect(response).to redirect_to :action => :result
    end
  end

  describe '#result' do
    it 'should return a message containing the word plant if the counters are equal' do
      session[:cat_counter] = 2
      session[:dog_counter] = 2
      get :result
      expect(assigns(:message)).to include("plant")
    end

    it 'should return a message containing the word cat if the cat_counter has more points' do
      session[:cat_counter] = 3
      session[:dog_counter] = 2
      get :result
      expect(assigns(:message)).to include("cat")
    end
    it 'should return a message containing the word dog if the dog_counter has more points' do
      session[:cat_counter] = 2
      session[:dog_counter] = 3
      get :result
      expect(assigns(:message)).to include("dog")
    end
  end

  describe '#error' do
    it 'should render the error template' do
      get :error
      expect(response).to render_template("error")
    end
  end

end