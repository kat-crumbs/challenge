require 'rails_helper'

RSpec.describe WelcomeController do

  describe '#index' do

    it 'should reset all variables' do
      @cat_counter = 2
      @dog_counter = 1
      @answered_questions = [3,2,5]
      get :index
      expect(assigns(:cat_counter)).to eq(0)
      expect(assigns(:dog_counter)).to eq(0)
      expect(assigns(:answered_questions)).to eq([])
    end

    it 'should render the index template' do
      get :index
      expect(response).to render_template("index")
    end
  end

end
