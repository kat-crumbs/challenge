require 'rails_helper'

RSpec.describe Question do

  describe '#self.not_included_in' do
    it 'should give back only a questions that was not answered yet' do
      @answered_questions = [3,5,2,4,6]
      expect(Question.not_included_in(@answered_questions).id).to eq(1)
      @answered_questions = [1,5,2,4,6]
      expect(Question.not_included_in(@answered_questions).id).to eq(3)
    end
  end

end