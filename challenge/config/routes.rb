Rails.application.routes.draw do

  get 'questions/index'
  get 'questions/decision/:id/:decision' => "questions#decision", :as => :decision
  get 'questions/result'
  get 'questions/error'
  root 'welcome#index'

end
