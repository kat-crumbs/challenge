# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(title: 'cat')
Category.create(title: 'dog')
Question.create(body: 'Do you want to build the next Social Network for Cats?', category_id: 1)
Question.create(body: 'Do you think dogs should be allowed everywhere people are?', category_id: 2)
Question.create(body: 'Your middle name is “Meow"', category_id: 1)
Question.create(body: 'Is Snoop Dog one of your favourite artists?', category_id: 2)
Question.create(body: 'Do you spend all day watching cat videows?', category_id: 1)
Question.create(body: 'Do you stop people in the street to pet their dogs?', category_id: 2)
