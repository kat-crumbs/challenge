class Question < ActiveRecord::Base
  belongs_to :category

  def self.not_included_in(answered_questions)
    where('id NOT IN(?)', answered_questions).sample
  end

end
