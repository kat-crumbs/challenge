class QuestionsController < ApplicationController

  def index
    begin
      @question = if @answered_questions.length > 0
        Question.not_included_in(@answered_questions)
      else
        Question.all.sample
      end
      @answered_questions << @question.id
      @text = @question.body
      @current_id = @question.category_id
    rescue NoMethodError
      redirect_to action: 'error', status: 303
    end
  end

  def decision
    @id = params[:id].to_i
    @decision = params[:decision]
    if @id == 2 && @decision == "yes" || @id == 1 && @decision == "no"
      @dog_counter += 1
    elsif @id == 1 && @decision == "yes" || @id == 2 && @decision == "no"
      @cat_counter += 1
    end
    if @answered_questions.length == 3
      redirect_to action: 'result', status: 303
    else
      redirect_to action: 'index', status: 303
    end
  end

  def result
    if @dog_counter > @cat_counter
      @message = "...like dogs more than anything else!"
    elsif @dog_counter < @cat_counter
      @message = "...love cats more than anything else!"
    else @dog_counter = @cat_counter
      @message = "...are more of a plant person."
    end
  end

  def error
  end

end