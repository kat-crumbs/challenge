class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_filter :load_dog_counter, :load_cat_counter, :load_questions
  after_filter :save_dog_counter, :save_cat_counter, :save_questions

  private

  def load_dog_counter
    @dog_counter = session[:dog_counter] ||= 0
  end

  def save_dog_counter
    session[:dog_counter] = @dog_counter
  end

  def load_cat_counter
    @cat_counter = session[:cat_counter] ||= 0
  end

  def save_cat_counter
    session[:cat_counter] = @cat_counter
  end

  def load_questions
    @answered_questions = session[:answered_questions] ||= []
  end

  def save_questions
    session[:answered_questions] = @answered_questions
  end

end





